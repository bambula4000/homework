"""
Chess Board
This script takes 2 parameter(width and height) and printing chess board of asterisk and space symbols.
"""


def validation_input_data(text):
    while True:
        try:
            board_size = int(input('Enter ' + text + ' of chess board:\n'))
            if board_size > 0:
                break
            elif board_size < 1:
                print('Please enter a digit bigger then 0')
        except ValueError:
            print('You write a string! Please enter a digit.')
    return board_size


def make_board(height, width):
    complete_board = []
    for i in range(height):
        boards_row = []
        if i % 2 == 0:
            for j in range(width):
                if j % 2 == 0:
                    boards_row.append('*')
                else:
                    boards_row.append(' ')

        if i % 2 != 0:
            for k in range(width):
                if k % 2 == 0:
                    boards_row.append(' ')
                else:
                    boards_row.append('*')

        complete_board.append(boards_row)
    return complete_board


if __name__ == "__main__":
    width_of_board = validation_input_data('width')
    height_of_board = validation_input_data('height')
    complete_board = make_board(height_of_board, width_of_board)

    for board_part in complete_board:
        print(*board_part, sep='')

