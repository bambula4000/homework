"""
This script is checks how many palindromes is in wrote string.
It returns all founded palindromes, or 0 if nothing is found
"""


def found_palindrome():
    user_string = input("Please, input data. It can be any symbol, letters or digits:\n")
    palindroms = []

    for finish_char in range(len(user_string)):
        for start_char in range(finish_char):
            palindrome = user_string[start_char:finish_char + 1]
            if palindrome == palindrome[::-1]:
                palindroms.append(palindrome)
    if len(palindroms) > 0:
        return palindroms
    else:
        palindroms.append(0)
        return palindroms


if __name__ == "__main__":
    all_palindroms = found_palindrome()
    for palindrome in set(all_palindroms):
        print(palindrome)
