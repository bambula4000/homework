from math import sqrt


def input_data(text: str)-> int:
    return int(input(text))


def validation_data(text: str) -> int:
    while True:
        try:
            data = input_data(text)
            if data > 0:
                return data
        except ValueError:
            print("Incorrect input! Please write a digit.")
            continue
        print("Please write a digit bigger than 0")


def compute_square_root(square: int)-> int:
    return int(sqrt(square))


def create_sequence(row_length: int, square: int)-> list:
    square_roots_sequence = list()
    min_square_root = compute_square_root(square)

    for square_root in range(row_length):
        min_square_root += 1
        square_roots_sequence.append(min_square_root * min_square_root)
    return square_roots_sequence


if __name__ == "__main__":
    print(
        create_sequence(
            validation_data("Enter row length: "),
            validation_data("Enter minimal square: "),
            )
        )
