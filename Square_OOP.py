from math import sqrt
from typing import Any
from abc import ABC, abstractmethod


class AskIntInput:
    def __init__(self, question: str):
        self._question = question

    def value(self) -> int:
        while True:
            try:
                return int(input(f"{self._question} \n"))
            except ValueError:
                print("Incorrect input. Please enter a digit")


class Requirement(ABC):
    @abstractmethod
    def passed(self, value: Any):
        pass


class BiggerThenZero(Requirement):
    def passed(self, value: Any)-> bool:
        return value > 0


class SmallerThanMillion(Requirement):
    def passed(self, value: Any)-> bool:
        return value < 100000


class CheckData(Requirement):
    def __init__(self, *requirements: Requirement):
        self._checks = requirements

    def passed(self, value: Any)-> bool:
        for requirement in self._checks:
            if not requirement.passed(value):
                print(f'{requirement.__class__.__name__} checks is failed. Enter correct data!')
                return False
        return True


class CalculateSquareRoot:
    def __init__(self, length: int, square: int):
        self._length = length
        self._square = square

    def compute_square_root(self)-> int:
        return int(sqrt(self._square))

    def create_sequence(self)-> list:
        sequence = list()
        minimal_square_root = self.compute_square_root()
        for _ in range(self._length):
            minimal_square_root += 1
            sequence.append(minimal_square_root * minimal_square_root)
        return sequence


if __name__ == '__main__':
    while True:
        sequence_length = AskIntInput('Please enter a sequence length between 1 to 999999: ').value()
        digit_to_compute_square_root = AskIntInput('Please enter a minimal square root: ').value()

        if CheckData(
            BiggerThenZero(),
            SmallerThanMillion()
        ).passed(sequence_length) and CheckData(
                                        BiggerThenZero(),
                                      ).passed(digit_to_compute_square_root):
            sequence_of_square_roots = CalculateSquareRoot(sequence_length, digit_to_compute_square_root)
            print(sequence_of_square_roots.create_sequence())
            break
