from math import sqrt

def validation_input_data(text):
    while True:
        try:
            data = int(input(text + ' :\n'))
            if data > 0:
                break
            elif data < 1:
                print("Please write a digit bigger than 0")
        except ValueError:
            print("Incorrect input! Please write a digit.")
    return data


def compute_roots(row_length, min_sqrt):
    square_roots_array = []
    min_sqrt = int(sqrt(min_sqrt))
    for sqrt_root in range(row_length):
        square_roots_array.append(min_sqrt)
        min_sqrt += 1
    return square_roots_array


if __name__ == "__main__":
    print(compute_roots(validation("Enter row lenght"), validation("Enter minimal square")))
